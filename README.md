# unit_test

Example of using gitlab pipelines for unit testing in python 

## Purpose

Unit testing is great for testing functions in a software project.  
Gitlab CI adds to the unit testing concept by triggering unit test automatically when pushing to a repository.

## What is going on in this project ?

This project has 4 files.

1. *functions.py* has one simple function that adds 2 numbers and returns the result
2. *main.py* is the main program that imports the functions file and uses it
3. *test.py* is using the standard python library called unittest. It imports the functions file and defines 2 test cases.  
The first to assert that 2+2 equals 4 when using the add function.  
The second case to assert the 2+2 does not equal 5 when using the add function.  
Documentation for the unittest library is here: [https://docs.python.org/3/library/unittest.html#test-cases](https://docs.python.org/3/library/unittest.html#test-cases)

4. *.gitlab-ci.yml* is a configuration for gitlabs continious integration (CI) pipeline in `yaml` format.  
gitlabs CI is capable of running docker containers with different images. The pipeline is triggered when pushing to the gitlab repository. 
The specified pipeline job is using a docker image with a linux+python image from dockerhub [https://hub.docker.com/_/python](https://hub.docker.com/_/python)  
After the image is pulled from dockerhub the *test.py* script is executed inside the docker container.  
To view the test results go to the pipeline tab on gitlab.

## Bonus

If one or more of the CI tests fail, it will fail the CI job + following jobs on gitlab.  
This is particuarly cool if you have multiple jobs (stages) in your gitlab pipeline because the following jobs will not run.  
Effectively this means that broken code will not be allowed into the code base.  

*test.py* contains a commented test case. Try to uncomment it and push your changes to see how gitlab CI behaves on a failed test case. 

## Tips

* Put your functions in one or more seperate files.  
* Write small functions that only does one thing. It is easier to test, read and maintain small functions.
